def search_vowels(phrase: str) -> set:  # word:str аннотация указывает, что в аргум. word функция должна получать строку
    # ->set указывает, что функция возвращает множество
    """Выводит гласные, найденные во введенном слове."""
    vowels = set('аяоеуюыиэе')
    return vowels.intersection(set(phrase))


def search_letters(phrase: str, letters: str='аяоеуюыиэе') -> set:
    """Возвращает множество букв из letters, найденных в указанной фразе."""
    return set(letters).intersection(set(phrase))
    # создается объект множества из letters и находим пересечение множества