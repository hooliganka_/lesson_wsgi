from views import Home, Search, Stat, Bloks, Test

patterns = (
    (r'^/$', Home('index.html')),
    (r'^/search$', Search('result.html')),
    (r'^/stat$', Stat('statistika.html')),
    (r'^/blok$', Bloks('mew_block.html')),
    (r'^/test$', Test('test.html')),
    # (r'^/res$', Results('res.html')),
)
