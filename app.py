from core.result_url import result


def app(request, start_response):
    start_response('200 OK', [('Content-Type', 'text/html')])
    return result(request)


if __name__ == '__main__':
    try:
        from wsgiref.simple_server import make_server
        httpd = make_server('', 8080, app)
        print('Serving on port 8080...')
        httpd.serve_forever()
    except KeyboardInterrupt:
        print('Goodbye.')