from jinja2 import Template

x = """
{% set test = True %}

{% if test %}
    Hello {{ name }}
{% else %}
    Bay {{ name }}
{% endif %}
"""
print(x)
t = Template(x)

complate = t.render({
    # 'test': False,
    'name': 'Nastya',
})

print(complate)
