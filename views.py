import json
import random
import sqlite3
from core.basedb import database_name
from core.myviews import MyViews
from search import search_vowels
from PIL import Image, ImageDraw, ImageEnhance


class Home(MyViews):
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)


class Search(MyViews):
    def search_vowels(
            phrase: str) -> set:  # word:str аннотация указывает, что в аргум. word функция должна получать строку
        # ->set указывает, что функция возвращает множество
        """Выводит гласные, найденные во введенном слове."""
        vowels = set('аяоеуюыиэе')
        return vowels.intersection(set(phrase))

    def get(self, request, *args, **kwargs):
        # print('Search', request['GET']['phrase'])
        glas_buk = search_vowels(request['GET']['phrase'])
        i = ''.join(glas_buk)
        # print('search_vowels: ', i)
        request['the_phrase'] = request['GET']['phrase']
        request['the_letters'] = 'аяоеуюыиэ'
        request['results'] = i
        connect = sqlite3.connect(database_name)
        cursor = connect.cursor()
        cursor.execute(
            "INSERT INTO 'slova' (`name`, `bukvi`) "
            "VALUES ('%s', '%s')" % (
                request['GET']['phrase'],
                i,
            )
        )
        connect.commit()
        connect.close()
        return super().get(request, *args, **kwargs)

        # def post(self, request, *args, **kwargs):
        #     sl = request['POST']['phrase']
        #     glas_buk = search_vowels(request['POST']['phrase'])
        #     i = ''.join(glas_buk)
        #     print('slovo: ', sl)
        #     print('bukvi: ', i)
        #     return super().post(request, *args, **kwargs)


class Stat(MyViews):
    def get(self, request, *args, **kwargs):
        con = sqlite3.connect(database_name)
        cur = con.cursor()
        cur.execute("""SELECT * FROM `slova`""")
        request['rows'] = cur.fetchall()
        # print(request['rows'])
        con.commit()
        for item in request['rows']:
            request['id'] = item[0]
            request['slova'] = item[1]
            request['byk'] = item[2]
            # print('id', request['id'])
            # print('slova', request['slova'])
            # print('byk', request['byk'])
            # print(request['id'], request['slova'], request['byk'])
            # return request['id'], request['slova'], request['byk']

            return super().get(request, *args, **kwargs)


class Bloks(MyViews):
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        # data = json.loads(request['POST'].get('data'))
        # data = [[0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        #         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        #         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        #         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        #         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        #         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        #         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        #         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        #         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        #         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        #         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        #         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        #         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        #         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        #         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        #         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        #         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        #         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        #         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        #         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]
        # data = [[random.randint(0, 1) for i in range(20)] for item in range(20)]
        data = []
        t = 0
        for i in range(20):
            t += 1
            data.append([])
            for j in range(20):
                data[i].append(random.randint(0, 0))
                if j == 10:
                    data[i].append(1)
                if t == 5:
                    data[i].append(1)
            t = 0
        print(data)
        image = Image.new('RGBA', (1000, 1000))
        p = 0
        k = 0
        for y in data:
            p += 1
            for x in y:
                if x == 0:
                    image.paste(Image.new('RGB', (50, 50), (0, 140, 240)), (50 * k, 50 * p))
                else:
                    image.paste(Image.new('RGB', (50, 50), (255, 0, 0)), (50 * k, 50 * p))
                k += 1
            k = 0
        image.save('sample.png')
        return super().get(request, *args, **kwargs)


class Test(MyViews):
    def get(self, request, *args, **kwargs):
        mat = [[0, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
               [1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
               [0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
               [0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
               [0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
               [0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
               [0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
               [0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0],
               [0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0],
               [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0],
               [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0],
               [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0],
               [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0],
               [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
               [0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
               [0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
               [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
               [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
               [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
               [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
               ]
        divs = ['<div style="background: red;"></div>', '<div style="background: aqua;"></div>']
        temp = []
        for item in mat:
            for i in item:
                if i == 1:
                    rew = divs[0]
                else:
                    rew = divs[1]
                temp.append(rew)
                r = ' '.join(str(e) for e in temp)
                request['block'] = r
        return super().get(request, *args, **kwargs)
