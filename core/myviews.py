from jinja2 import Template
from urllib.parse import parse_qsl


def qs_parser(qs):
    result = {}
    for row in parse_qsl(qs):
        result[row[0]] = row[1]
    return result


class MyViews:

    def __init__(self, template):
        self.template = template

    def get(self, request, *args, **kwargs):
        with open('templates/%s' % self.template, 'r', encoding='utf-8') as i:
            response = i.read()  # читаем
            templates = Template(response)
            response = templates.render(request)
            return response

    def post(self, request, *args, **kwargs):
        with open('templates/%s' % self.template, 'r', encoding='utf-8') as i:
            response = i.read()
            templates = Template(response)
            response = templates.render(request)
            return response

    def put(self, request, *args, **kwargs):
        pass

    def delete(self, request, *args, **kwargs):
        pass

    def view(self, request, *args, **kwargs):
        if request['REQUEST_METHOD'] == 'GET':
            request['GET'] = qs_parser(request['QUERY_STRING'])
            return self.get(request, *args, **kwargs)

        elif request['REQUEST_METHOD'] == 'POST':
            request['POST'] = qs_parser(request['wsgi.input'].read(
                int(request.get('CONTENT_LENGTH', 0))).decode())
            return self.post(request, *args, **kwargs)

        elif request['REQUEST_METHOD'] == 'PUT':
            request['PUT'] = qs_parser(request['wsgi.input'].read(
                int(request.get('CONTENT_LENGTH', 0))).decode())

        elif request['REQUEST_METHOD'] == 'DELETE':
            request['DELETE'] = qs_parser(request['wsgi.input'].read(
                int(request.get('CONTENT_LENGTH', 0))).decode())
