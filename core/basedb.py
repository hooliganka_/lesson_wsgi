import os
import sqlite3

database_name = 'my.db'


class Basebd:
    if not os.path.exists(database_name):
        conn = sqlite3.connect(database_name)
        c = conn.cursor()
        # Создание таблицы
        c.execute('''CREATE TABLE IF NOT EXISTS `slova` (`id` INTEGER PRIMARY KEY AUTOINCREMENT,
                                                        `name` VARCHAR, 
                                                        `bukvi` VARCHAR)''')
        # Подтверждение отправки данных в базу
        conn.commit()
        # Завершение соединения
        c.close()
        # Закрытие
        conn.close()
