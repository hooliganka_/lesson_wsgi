import re
from url import patterns


def result(request, *args, **kwargs):
    responses = ''
    for url in patterns:
        match = re.search(url[0], request['PATH_INFO'])  # проверяем совподение url
        if match:  # match если true
            # test = {}
            if url[1]:  # url[2] если true
                responses = url[1].view(request, *args, **kwargs)
            # if url[1]:  # url[1] если true
            #     i = open('templates/%s' % url[1], 'r', encoding='utf-8')  # открываем файл
            #     response = i.read()  # читаем
            #     templates = Template(response)
            #     response = templates.render(**test)
            # else:
            #     response = result
    return [responses.encode()]